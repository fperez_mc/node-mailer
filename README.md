# Node mail templates

Sample app to send emails and preview handlebars templates (local).

### **Before start:**

First, on root create `.env` file with:
```javascript
SERVICE=email_provider // 'gmail','hotmail'
EMAIL=user@.mail.com
PASSWORD=P4sW0rd
PORT=3000
```

Then, on `app/index.js` complete: service / auth and email info (from/to)
```javascript
let transporter = nodemailer.createTransport({
    service: process.env.SERVICE
    port: 587,
    secure: false,
    auth: {
        user: proces.env.EMAIL,
        pass: proces.env.PASSWORD
    }
});

transporter.sendMail({
    from: proces.env.EMAIL,
    to: 'test@mail.com', // where you receive the compiled template
    subject: 'Subject for email',
    html: templateCompiled // compiled template from middleware
});

```

---

### **Next steps:**

Open terminal or bash and run: `npm start`
 
Go to `http://localhost:3000/preview` to preview the template.

Go to `http://localhost:3000/send` to send email with the template.

Try to run `npm run send` > `curl -X GET http://localhost:3000/send`

If you want to load another template check: `app/middlewares/index.js`.

If you want to send differents options check: `app/util/rfs.js`.

Check the console for details about status/errors.

---

### **Util links:**

[Photopea](https://www.photopea.com/) (quick image)

[Nodemailer](https://nodemailer.com/message/) (to send emails)

[Handlebars](https://handlebarsjs.com/api-reference/runtime-options.html#options-to-control-prototype-access) (dynamic templates)

[Google Drive](https://support.google.com/drive/thread/34363118?hl=en) (for embed images)

[Dotenv](https://www.youtube.com/watch?v=Va9UKGs1bwI) (enable Google permissions)

[TO BASE64](https://www.base64-image.de/) (convert images to base64)
