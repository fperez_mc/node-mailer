// Node.js dependencies
const path = require('path');
const fs = require('fs');

// Third party lib
const handlebars = require('handlebars');

// Utils
const rfs = require('../util/rfs');

// Middlewares
function loadObj(req, res, next) {
    res.locals.loadObj = rfs;
    console.log('✔ rfs loaded');
    next();
}

function tableRead(req, res, next) {
    for (let i = 1; i < 11; i++) {
        let pathAbs = path.join(__dirname, `../templates/table_partials/table_${i}.handlebars`);
        fs.readFile(pathAbs, {encoding: 'utf-8'}, function(err, table) {
            handlebars.registerPartial(`table_${i}`, table);
        });
    }
    console.log('✔ tables read');
    next();
};

function read(req, res, next) {
    const pathAbs = path.join(__dirname, '../templates/template_07.html');
    fs.readFile(pathAbs, {encoding: 'utf-8'}, function(err, html) {
        res.locals.template = html;
        console.log('✔ file read');
        next();
    });
};

function compile(req, res, next) {
    const compiledTemp = handlebars.compile(res.locals.template);
    res.locals.parsedTemp = compiledTemp(res.locals.loadObj);
    console.log('✔ template compiled\n');
    next();
};

module.exports = {
    loadObj,
    tableRead,
    read,
    compile
};