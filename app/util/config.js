// Config constants for templates

// itProcConf, hierarchicalAp, vendorEval, financialAp, sap, requisition, itCostManVal, closed
const colors = {
    itProcConf: '#e8c973',
    hierarchicalAp: '#73d0e8',
    areaApproval: '#73aae8',
    vendorEval: '#e5ab38',
    financialAp: '#9fc975',
    sap: '#b673e8',
    requisition: '#e4a2ee',
    itCostManVal: '#75c9b2',
    closed: '#bcbcbc'
};

// draft, itProcConf, hierarchicalAp, areaApproval, vendorEval, financialAp, sap, requisition, itCostManVal, closed
const statusRfs = {
    draft: 'Draft',
    itProcConf: 'IT PROC Confirmation',
    hierarchicalAp: 'Hierarchical Approval',
    areaApproval: 'Area Approval',
    vendorEval: 'Vendor Evaluation',
    financialAp: 'Financial Approval',
    sap: 'SAP Info Update',
    requisition: 'Requisition',
    itCostManVal: 'IT Cost Mgmt. Validation',
    closed: 'Closed',
    comment: 'Requisition New Comment'
};

const labels = [
    'Submitted',
    'Created',
    'Assigned',
    'Assigned for Approval',
    'Assigned for Validation',
    'Approved',
    'Vendor Selection Completed',
    'Financial Approved',
    'SAP Information',
    'Requisition ID',
    'PR / PO Process Required',
    'Closed / Cancelled / Rejected',
    'Requisition New Comment'
];

const contentText = [
    'The RFS below has been created/submitted to the IT Procurement team who will review your request within\xa048\xa0hours.',
    'The RFS below has been assigned to you by XX (Validate)',
    'The RFS below has been assigned for your approval:',
    'The RFS below has been assigned for your validation:',
    'The RFS below has been approved by XXX (User ID)',
    'The RFS below has been approved and the Vendor Selection has been completed.',
    'The RFS below has been assigned for your approval:',
    'The RFS below has been approved by XXX (User ID)',
    'The RFS below has been assigned to you in order to complete the SAP Information required:',
    'The RFS below has been assigned to you in order to complete the Requisition ID required:',
    'The RFS below has been commented:',
    'The RFS below has been assigned to you in order to complete the PR / PO Process required:',
    'No action is required. The service request below has reached the Closed (Cancelled / Rejected) phase.'
];

const bottomText = [
    'For additional details, please click the link below to open the request:\xa0(Open\xa0in\xa0Chrome)',
    'In order to approve or reject please click below:\xa0(Open\xa0in\xa0Chrome)',
    'In order to Send Back to Approvals / Confirm Rejection approve or reject please click below:\xa0(Open\xa0in\xa0Chrome)'
];

const emailTitles = [
    'RFS IDNum has been created - submitted',
    'RFS IDNum has been assigned to you',
    'RFS IDNum has been assigned for your approval',
    'RFS IDNum has been assigned for your validation',
    'RFS IDNum has been approved',
    'RFS IDNum has been evaluated – Vendor Selection Completed',
    'RFS IDNum has been assigned for Financial approval',
    'RFS IDNum has been Financial approved',
    'RFS IDNum has been assigned to complete SAP Info',
    'RFS IDNum has been assigned to complete the Requisition ID',
    'RFS IDNum has been Closed (Cancelled,Rejected)'
];

// draft, itProcConf, hierarchicalAp, areaApproval, vendorEval, financialAp, sap, requisition, itCostManVal, closed
const statusTable = {
    draft: 'table_1',
    itProcConf: 'table_2',
    hierarchicalAp: 'table_3',
    areaApproval: 'table_4',
    vendorEval: 'table_5',
    financialAp: 'table_6',
    sap: 'table_7',
    requisition: 'table_8',
    itCostManVal: 'table_9',
    closed: 'table_10'
};

module.exports = {
    colors,
    statusRfs,
    labels,
    contentText,
    bottomText,
    emailTitles,
    statusTable
};