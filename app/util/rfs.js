// Config file
const config = require('./config');

// RFS array of objects
const rfs = [
    {
        emailSubject: config.emailTitles[0],
        color: config.colors.itProcConf,
        status: config.statusRfs.itProcConf,
        label: config.labels[0],
        contentText: config.contentText[0],
        rfs: {
            id: '46',
            title: 'Stealthwatch',
            requestor: 'Alexander, Mathew',
            status: 'IT Procurement Validation',
            categoryManager: 'Guido Ontaneda (B92167)',
            cityAndCountry: 'Neenah, WI',
            organization: 'Kimberly-Clark Corporation',
            category: 'Software , Hardware , Service',
            region: 'Global'
        },
        showCta: true,
        isAction: true,
        actionText: config.bottomText[1],
        notificationText: config.bottomText[0],
        link: {
            goToRfs: `https://dtsproc.kcc.com/request/46`,
            approve: `https://dtsproc.kcc.com/request/46/approve`,
            reject: `https://dtsproc.kcc.com/request/46/reject`,
        },
        statusTable: config.statusTable.draft
    },
    {
        emailSubject: config.emailTitles[2],
        color: config.colors.hierarchicalAp,
        status: config.statusRfs.hierarchicalAp,
        label: config.labels[2],
        contentText: config.contentText[2],
        rfs: {
            id: '46',
            title: 'Stealthwatch',
            requestor: 'Alexander, Mathew',
            status: 'IT Procurement Validation',
            categoryManager: 'Guido Ontaneda (B92167)',
            cityAndCountry: 'Neenah, WI',
            organization: 'Kimberly-Clark Corporation',
            category: 'Software , Hardware , Service',
            region: 'Global'
        },
        showCta: true,
        isAction: false,
        actionText: config.bottomText[1],
        notificationText: config.bottomText[0],
        link: {
            goToRfs: `https://dtsproc.kcc.com/request/46`,
            approve: `https://dtsproc.kcc.com/request/46/approve`,
            reject: `https://dtsproc.kcc.com/request/46/reject`,
        },
        statusTable: config.statusTable.hierarchicalAp
    },
    {
        emailSubject: config.emailTitles[9],
        color: config.colors.requisition,
        status: config.statusRfs.requisition,
        label: config.labels[12],
        contentText: config.contentText[10],
        rfs: {
            id: '46',
            title: 'Stealthwatch',
            requestor: 'Alexander, Mathew',
            status: 'IT Procurement Validation',
            categoryManager: 'Guido Ontaneda (B92167)',
            cityAndCountry: 'Neenah, WI',
            organization: 'Kimberly-Clark Corporation',
            category: 'Software , Hardware , Service',
            region: 'Global',
            comment: 'This is a comment example.'
        },
        showCta: true,
        isAction: false,
        actionText: config.bottomText[1],
        notificationText: config.bottomText[0],
        link: {
            goToRfs: `https://dtsproc.kcc.com/request/46`,
            approve: `https://dtsproc.kcc.com/request/46/approve`,
            reject: `https://dtsproc.kcc.com/request/46/reject`,
        },
        statusTable: config.statusTable.requisition
    }
];

// Exports only one by index
module.exports = rfs[2];