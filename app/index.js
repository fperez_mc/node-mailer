// ENV config
require('dotenv').config();

// Third party lib
const nodemailer = require('nodemailer');
const express = require('express');
const favicon = require('serve-favicon');
const path = require('path');

// Config
const app = express();
const port = process.env.PORT;

// Middlewares
const mdw = require('./middlewares/index');
app.use(mdw.loadObj);
app.use(mdw.tableRead);
app.use(mdw.read);
app.use(mdw.compile);

app.use(favicon(path.join(__dirname, 'assets/img', 'favicon.ico')));

// Endpoints
app.get('/', (req, res) => {
    res.redirect('/preview');
});

app.get('/preview', (req, res) => {
    res.writeHeader(200, {"Content-Type": "text/html"});
    res.write(res.locals.parsedTemp);
    res.end();
});

app.get('/send', (req, res) => {
    // reusable transport obj SMTP
    let transporter = nodemailer.createTransport({
        service: process.env.SERVICE,
        auth: {
            user: process.env.EMAIL,
            pass: process.env.PASSWORD
        }
    });
    // verify connection configuration
    transporter.verify(function(error, success) {
        if (error) {
            console.log(error);
            res.write('✘ Error with the email');
            res.redirect('/preview');
        } else {
            // send mail with transport obj
            transporter.sendMail({
                from: `DTSPS ${process.env.EMAIL}`,
                to: `federico.perez@kcc.com`,
                subject: `${res.locals.loadObj.emailSubject}`,
                html: res.locals.parsedTemp
            });
            console.log(`\n ✔ email sent`);
            console.log('go to preview... \n');
            res.redirect('/preview');
        }
    });
});

app.listen(port, () => {
  console.log(`✔ Server ready on http://localhost:${port}`);
});
